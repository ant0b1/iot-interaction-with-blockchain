# IoT interaction with Blockchain


- The project consist in a simple temperature monitoring application
- Has been used a esp32 that host temperature sensor and a card reader.
- The temperature will be read and after sended when an authorized card has been read by card reader.
- The communication between client and server is performed through TLS.

Hyperledger Fabric is an open source, permissioned blockchain framework.It is a modular, general-purpose framework that offers unique identity management and access control features.
Fabric networks are permissioned, meaning all participating member’s identities are known and authenticated.
Fabric networks consist of channels, which are a private “subnet” of communication between two or more specific network members, members on the network can transact in a private and confidential way. Each transaction on the blockchain network is executed on a channel, where each party must be authenticated and authorized to transact on that channel.


